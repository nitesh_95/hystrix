package com.bhushan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controllers {

	@Autowired
	private ResTemplateService resTemplateService;

	@GetMapping("/getall")
	public String getemployees() {
		String getdata = resTemplateService.getdata();
		return getdata;
	}

}
